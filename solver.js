if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/ticket-solver/sw.js').then(function(reg) {
    console.log('Registration succeeded. Scope is ' + reg.scope);
  });
}

function _getTargetValue() {
  const iPrice = document.getElementById("input_price").value;
  return Number.parseFloat(iPrice);
}

function _getValues() {
  const sValues = document.getElementById("input_values").value;
  const iValues = sValues.split(";").map(n => Number.parseInt(n));
  return iValues;
}

function _computeR(aCurrent, currentValue, mapResults, iValues, iTarget, k) {
  // console.log("aCurrent", aCurrent,
  //             "value", currentValue, 
  //             "results", mapResults,
  //             "k", k);
  //  mapInsertList(mapResults, currentValue, aCurrent);

  if (k >= iValues.length) {
    console.log("aCurrent", aCurrent,
      "value", currentValue,
      "results", mapResults,
      "k", k);
    mapInsertList(mapResults, currentValue, aCurrent);
    return;
  }
  if (currentValue > iTarget) return;

  let newValue = 0;
  for (let i = 0; newValue < iTarget; i++) {
    aCurrent[k] = i;
    newValue = evaluate(aCurrent, iValues);
    _computeR(aCurrent, newValue, mapResults, iValues, iTarget, k + 1);
  }
  aCurrent[k] = 0;
}

function mapInsertList(map, key, value) {
  if (map[key] == undefined) {
    map[key] = Array.of([...value]);
  } else {
    map[key].push([...value]);
  }
}

function evaluate(aCurrent, iValues) {
  let sum = 0;
  for (let i = 0; i < iValues.length; i++) {
    sum += aCurrent[i] * iValues[i];
  }

  return sum;
}

function zip(a, b) {
  return a.map((el, index) => [el, b[index]]);
}

function formatResult(aResult, iValues, sCurrencySymbol) {
  return zip(aResult, iValues)
    .filter(([count, value]) => count > 0)
    .map(([count, value]) => [`${count}`, `${value} ${sCurrencySymbol}`]);
}

function compute() {
  const iValues = _getValues();
  const nValues = iValues.length;
  const targetValue = _getTargetValue();

  let results = {};
  _computeR(Array(nValues).fill(0), 0, results, iValues, targetValue, 0);

  clearTable();

  for (let totalValue = targetValue; totalValue > 0; totalValue--) {
    if (results[totalValue] == undefined) { continue; }

    for (let result of results[totalValue]) {
      console.log("FINAL RESULT:", "k", totalValue, "result", result, formatResult(result, iValues, "€"));
      makeResultTable(formatResult(result, iValues, "€"), totalValue);
    }
    break;
  }
}

function clearTable() {
  const table = document.getElementById("result_table");
  table.innerHTML = "";
}

function addRow(table, first, second, header = false) {
  console.log("addRow:", first, second, header);
  const table_element = header ? "th" : "td";
  const tr = document.createElement("tr");
  const td_first = document.createElement(table_element);
  td_first.innerText = first;
  const td_second = document.createElement(table_element);
  td_second.innerText = second;

  if (header) {
    td_first.setAttribute("scope", "col");
    td_second.setAttribute("scope", "col");
  }

  table.appendChild(tr);
  tr.appendChild(td_first);
  tr.appendChild(td_second);
}

function makeResultTable(result, totalValue) {
  console.log("result:", result);
  const table = document.getElementById("result_table");
  const header = document.getElementById("result_header");

  header.innerText = `Total value: ${totalValue}`;
  addRow(table, "Count", "Value", true);

  for (let [count, value] of result) {
    console.log("count:", count, "value", value);
    addRow(table, count, value, false);
  }
}
