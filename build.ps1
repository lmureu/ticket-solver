$outputDir = "public"

Remove-Item -Recurse -Force "$outputDir"
mkdir "$outputDir"

Copy-Item -Path "*.png" -Destination "$outputDir" -Recurse
Copy-Item -Path "*.html" -Destination "$outputDir" -Recurse
Copy-Item -Path "*.js" -Destination "$outputDir" -Recurse
Copy-Item -Path "*.json" -Destination "$outputDir" -Recurse

$basedir=(Get-Item .).Name
$date = Get-Date -format "FileDateTimeUniversal"
$cached = (Get-ChildItem *.png,*.js,*.html).name `
    -Replace "^","`"/$basedir/" `
    -Replace "$","`",`n"

(Get-Content .\sw.js.template) `
    -Replace "{cached_files}","$cached" `
    -Replace "{deploy_date}","$date" `
    | Set-Content $outputDir\sw.js
